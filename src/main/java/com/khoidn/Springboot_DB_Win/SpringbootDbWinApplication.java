package com.khoidn.Springboot_DB_Win;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringbootDbWinApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringbootDbWinApplication.class, args);
	}

}
