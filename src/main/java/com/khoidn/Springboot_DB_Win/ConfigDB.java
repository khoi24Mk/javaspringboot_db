package com.khoidn.Springboot_DB_Win;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.khoidn.Springboot_DB_Win.dao.StudentDAO;
import com.khoidn.Springboot_DB_Win.entity.Student;

@Configuration
public class ConfigDB {

	@Bean // bean auto scan without @Autowire
	public CommandLineRunner commandLineRunner(StudentDAO studentDAO) {

		return runner -> {
			System.out.println("Hello to me");
			studentDAO.save(createStudent(studentDAO));
//			deleteByFirstName(studentDAO);
//			deleteStudent(studentDAO);
//			List<Student> listStudents = studentDAO.findByLastName("nguyen4");
//			List<Student> listStudents = studentDAO.findAll();
//			for (Student student : listStudents) {
//
//				System.out.println(student.toString());
//			}
		};
	}

	private void deleteByFirstName(StudentDAO studentDAO) {
//		String firstname = "khoi1";
		Student tempStudent = studentDAO.findByID(3000);
		studentDAO.deleteByFirstName(tempStudent);
	}

	private void updateStudent(StudentDAO studentDAO) {
		int studentId = 5;
		Student temStudent = studentDAO.findByID(studentId);
		temStudent.setFirstName("My");
		studentDAO.update(temStudent);
	}

	private void deleteStudent(StudentDAO studentDAO) {
		int studentId = 3004;
//		Student temStudent = studentDAO.findByID(studentId);
		studentDAO.delete(studentId);
	}

	public Student createStudent(StudentDAO studentDAO) {
		// create student object
		Student tempStudent = new Student("khoidn", "nguyen", "khoi@gmail.com");

		// display save id
		System.out.println(tempStudent.toString());

		return tempStudent;

	}

}