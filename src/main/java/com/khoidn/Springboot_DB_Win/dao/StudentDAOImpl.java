package com.khoidn.Springboot_DB_Win.dao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.khoidn.Springboot_DB_Win.entity.Student;

import jakarta.persistence.EntityManager;
import jakarta.persistence.TypedQuery;
import jakarta.transaction.Transactional;

@Repository
public class StudentDAOImpl implements StudentDAO {

	// define field for entity manager
	EntityManager entityManager;

	// inject entity manager using constructor injection
	@Autowired
	public StudentDAOImpl(EntityManager _EntityManager) {
		this.entityManager = _EntityManager;
	}

	// implement save method
	@Override
	@Transactional
	public void save(Student student) {
		entityManager.persist(student);
	}

	@Override
	public Student findByID(int studentID) {
		return entityManager.find(Student.class, studentID);
	}

	@Override
	public List<Student> findAll() {

		TypedQuery<Student> theQuery = entityManager.createQuery("from Student order by firstName desc", Student.class);
		return theQuery.getResultList();
	}

	@Override
	public List<Student> findByLastName(String lastNames) {
		TypedQuery<Student> theQuery = entityManager.createQuery("from Student where lastName=:data", Student.class);
		theQuery.setParameter("data", lastNames);

		return theQuery.getResultList();
	}

	@Override
	@Transactional
	public void update(Student theStudent) {
		entityManager.merge(theStudent);
	}

	@Override
	@Transactional
	public void delete(int id) {
		Student student = entityManager.find(Student.class, id);
		entityManager.remove(student);
	}

	@Override
	@Transactional
	public void deleteByFirstName(Student thestudent) {
//		Student student = entityManager.merge(thestudent);
		System.out.println(thestudent.getFirstName());
		int row = entityManager.createQuery("DELETE From Student where firstName=:data")
				.setParameter("data", thestudent.getFirstName()).executeUpdate();

	}

//	@Override
//	public List<Student> findByFirstName(String theFirstName) {
//		TypedQuery<Student> theQuery = entityManager.createQuery("From Student where firstName =:data", Student.class);
//		
//	}
}
