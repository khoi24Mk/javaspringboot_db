package com.khoidn.Springboot_DB_Win.dao;

import java.util.List;

import com.khoidn.Springboot_DB_Win.entity.Student;

public interface StudentDAO {

	public void save(Student student);

	public Student findByID(int studentID);

	public List<Student> findAll();

	public List<Student> findByLastName(String lastNames);

//	public List<Student> findByFirstName(String lastNames);

	public void update(Student theStudent);

	public void delete(int id);

	public void deleteByFirstName(Student student);
}
